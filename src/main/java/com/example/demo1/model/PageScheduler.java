package com.example.demo1.model;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PageScheduler {
    private int id;
    private List<String> templates;
    private List<String> backgrounds;

    public PageScheduler(int id, List<String> templates, List<String> backgrounds) {
        this.id = id;
        this.templates = templates;
        this.backgrounds = backgrounds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getTemplates() {
        return templates;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    public List<String> getBackgrounds() {
        return backgrounds;
    }

    public void setBackgrounds(List<String> backgrounds) {
        this.backgrounds = backgrounds;
    }
}
