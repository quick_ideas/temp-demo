package com.example.demo1.config;

import com.example.demo1.model.PageScheduler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class BasicConfiguration {

    @Bean
    public PageScheduler pageScheduler() {
        return new PageScheduler(0,
                Arrays.asList("index.html", "page1.html", "page2.html"),
                Arrays.asList("#53ce00", "#9400ce", "#00ceb6")
        );
    }
}
