package com.example.demo1.controller;

import com.example.demo1.model.PageScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class MainController {
    private static int counter = 0;
    private final PageScheduler pageScheduler;

    @Autowired
    public MainController(PageScheduler pageScheduler) {
        this.pageScheduler = pageScheduler;
    }

    @RequestMapping("/home")
    public String getHome(Model model) {
        System.out.println(++counter);
        model.addAttribute("id", counter);
        model.addAttribute("www", "home");
        model.addAttribute("backgroundColor", "#7272c5");
        model.addAttribute("marginLeft", "30vh");
        model.addAttribute("textContent", "<br>Ala ma <b>kota</b> a kot ma <u>Alę</u>");
        return "home.html";
    }

    @RequestMapping("/pages")
    public String pages(Model model) {
        List<String> templates = pageScheduler.getTemplates();
        int pageId = pageScheduler.getId();
        List<String> backgrounds = pageScheduler.getBackgrounds();

        model.addAttribute("pageId", pageId);
        System.out.println("pageId" + pageId);
        model.addAttribute("pages", templates);
        model.addAttribute("backgroundColor", backgrounds.get(pageId));
        model.addAttribute("www", "/pages");
        String template = templates.get(pageId);

        pageId++;
        if (pageId == templates.size()) {
            pageId = 0;
        }
        pageScheduler.setId(pageId);
        return template;
    }

    @RequestMapping("/")
    public String index() {
        return "start.html";
    }

}
